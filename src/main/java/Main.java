import org.fusesource.jansi.Ansi;

public class Main {
    public static void main(String[] args) {
        System.out.println(Ansi.ansi().fg(Ansi.Color.RED).a("Hello world").reset());
    }
}
